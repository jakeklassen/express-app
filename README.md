# Express App for Dokku

This repo is an example express application configured to work with dokku.

See associated blog [post](http://jakeklassen.com/post/deploying-a-node-app-on-digital-ocean-using-dokku/) for more details.
