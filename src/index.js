import express from 'express';
import packgeInfo from '../package.json';

const app = express();
// dokku uses port 5000 by default
const port = process.env.PORT || 3000;

app.get('/', (req, res) => res.json({
  message: 'o hai!',
  version: packgeInfo.version
}));

app.listen(port, (err) => {
  if (err) {
    console.error(err);
    process.exit();
  }

  console.log(`Server running on port ${port}`);
});
